__author__ = 'kozloffsky@gmail.com'

import requests
import json


class Client():
    __api__ = 'https://api.sproutvideo.com/v1/'

    def __init__(self, app):
        self._api_token = app.config['SPROUT_API_TOKEN']

    def get_headers(self):
        return {'content-type': 'application/json', ' SproutVideo-Api-Key': self._api_token}

    def get(self, url, params):
        return requests.get(url, headers=self.get_headers(), params=params)

    def post(self, url, data):
        return requests.post(url, json.dumps(data), headers=self.get_headers())

    def token_uploads(self):
        return self.post('token_uploads', {

        })