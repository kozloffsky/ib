import unittest
import sys
__author__ = 'kozloffsky@gmail.com'


def get_suite():
    import tests
    import tests.idea
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(tests.idea.IdeaTests))
    return suite


def run():
    runner = unittest.TextTestRunner()
    runner.run(get_suite())


if __name__ == '__main__':
    sys.path.insert(0, '/home/mike/google_appengine')
    import dev_appserver
    dev_appserver.fix_sys_path()
    import appengine_config
    run()
