import base64
from datetime import datetime
from functools import wraps
import logging
import os
import sys
from cloudstorage.errors import NotFoundError
from flask.ext.security.changeable import change_user_password
from google.appengine.ext import blobstore
from google.appengine.api.blobstore.blobstore import create_upload_url
from google.appengine.ext import deferred
from google.appengine.api import memcache

import cloudstorage
from flask import Flask, jsonify, request, abort
from flask.ctx import after_this_request
from flask.ext.admin.base import Admin
from flask.ext.admin.contrib.sqla.view import ModelView
from flask.ext.login import current_user
from flask.ext.security.core import Security
from flask.ext.security.datastore import SQLAlchemyUserDatastore
from flask.ext.security.decorators import auth_token_required, _check_token
from flask.ext.security.forms import LoginForm, RegisterForm, ChangePasswordForm
from flask.ext.security.registerable import register_user
from flask.ext.security.utils import url_for_security, login_user, logout_user, encrypt_password
from flask.ext.security.views import create_blueprint, _commit
from flask.ext.wtf.form import Form
from flask.helpers import send_file, make_response
from flask.json import loads
from flask.wrappers import Response
from itsdangerous import SignatureExpired, BadSignature
from flask.ext.admin import BaseView, expose
from sqlalchemy.sql.expression import distinct
from werkzeug.utils import secure_filename
from flask.json import dumps
from flask_mail import Mail
from model import orm
from model.forms import RegisterUserForm
from sqlalchemy.orm import joinedload
from sqlalchemy.sql.functions import count
from werkzeug.datastructures import MultiDict
from werkzeug.exceptions import NotFound
from werkzeug.http import parse_options_header
from wtforms.ext.sqlalchemy.orm import model_form
from wtforms.validators import ValidationError


from google.appengine.ext.webapp import blobstore_handlers

server_settings = {
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
    "cookie_secret": "61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
    "login_url": "/login",
    "debug": True  # Allows auto-reload of templates
}

_api_response_dict = {
    "status_code": 200,
    "error": False,
    "data": False
}

app = Flask(__name__)
app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_ECHO'] = True
app.config['SECURITY_TOKEN_AUTHENTICATION_HEADER'] = 'user_token'
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_REGISTER_URL'] = '/create_account'
#DISABLE EMAILS FOR NOW
app.config['SECURITY_SEND_REGISTER_EMAIL'] = True
app.config['SECURITY_SEND_PASSWORD_CHANGE_EMAIL'] = True
app.config['SECURITY_SEND_PASSWORD_RESET_NOTICE_EMAIL'] = True
#todo: fix password encription
#app.config['SECURITY_PASSWORD_SALT'] = None
#app.config['SECURITY_PASSWORD_HASH'] = 'bcrypt'

app.config['BUCKET_NAME'] = "cdn.ideasbritain.com"#"idbr-profile-images"

# After 'Create app'
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)

db = orm(app)

mail = Mail(app)


from model.entity import User, Role, UserProfile, Idea, Comment, Competition, Image, Watcher, Like, Follower, \
    track_comment, track_idea_create, UserFeed, track_watch_idea, generate_feeds, Video, KudosTransaction, IdeaAccount, \
    UserAccount, build_rating, RatingHistory, aggregate_profile_extra

user_datastore = SQLAlchemyUserDatastore(db.db, User, Role)
security = Security(app, user_datastore, register_blueprint=True)

admin = Admin(app)

class VideoView(BaseView):
    bucket_name = "cdn.ideasbritain.com"

    @expose('/')
    def index(self):
        return self.render('video.html')

    @expose(url='/upload', methods=['POST'])
    def upload(self):
        file = request.files['file']
        options = {}
        options['retry_params'] = cloudstorage.RetryParams(backoff_factor=1.1)
        options['content_type'] = 'video/mpeg4'
        options['options'] = {'x-goog-acl': 'public link'}
        path = '/' + self.bucket_name + '/' + str(secure_filename(file.filename))
        try:
            with cloudstorage.open(path, 'w', **options) as f:
                f.write(str(file))
                print jsonify({"success": True})
            return self.render('video.html',is_success = True)
        except Exception as e:
            logging.exception(e)
            return self.render('video.html',is_success = False)
        self.render('video.html',is_success = False)


    @expose('/list')
    def list(self):
        stats = cloudstorage.listbucket('/'+self.bucket_name, delimiter='/')
        return self.render('list.html', stats = stats)






admin.add_view(ModelView(Idea, orm.db.session))
admin.add_view(ModelView(UserProfile, orm.db.session))
admin.add_view(ModelView(Competition, orm.db.session))
admin.add_view(ModelView(User, orm.db.session))
admin.add_view(ModelView(KudosTransaction, orm.db.session))
admin.add_view(VideoView(name='video'))

def api_response(f):
    """
    Convert if possible result of decorated function to flask json response
    if something goes wrong it will generate error request.

    Supported exceptions are:
    1. ValidationException - if your form has errors then just raise it like this
        raise ValidationError(form.errors)

    :param f: function to decorate
    :return: decorator function
    """

    @wraps(f)
    def decorated(*args, **kwargs):
        # TODO: add errors support
        data = _api_response_dict.copy()
        if request.json:
            logging.debug('request json')
            logging.debug(dumps(request.json))
        try:
            data['data'] = f(*args, **kwargs)
            if 'status' in data['data'] and data['data']['status'] is 'error':
                data['error'] = True
                data['status_code'] = 500
        except ValidationError as ve:
            data['data'] = ve.message
            data['error'] = True
            data['status_code'] = 500
        except NotFound as nfe:
            data['error'] = True
            data['status_code'] = 404
        #except:
        #    _api_response_dict['data'] =  sys.exc_info()[0]
        #    _api_response_dict['error'] = True
        #    _api_response_dict['status_code'] = 500

        json = jsonify(data)
        logging.debug('api responce body')
        logging.debug(dumps(data))
        return json

    return decorated

def cached(f):
    @wraps(f)

    def decorated(*args, **kwargs):
        if _check_token():
            cuid = current_user.id
        else:
            cuid = -1

        key = "_finc_%s:_cache_" % f.__name__ + str(kwargs) + str(cuid) + str(request.args)
        data = memcache.get(key)
        if data is not None:
            logging.debug('Using cached value for %s' % key)
            return data

        data = f(*args, **kwargs)
        memcache.set(key, data, 1000)
        return data

    return decorated


@app.route('/')
@api_response
def index():
    from model.entity import User
    user = User(email="test@test.sa")
    orm.db.session.add(user)
    orm.db.session.commit()
    return user.to_dict()

@app.route('/test')
@auth_token_required
@api_response
def test_secure_view():
    return {"result": "ok"}

@app.route('/api/login', methods=['POST','PUT'])
@api_response
def login():
    profile = None
    try:
        nickname = request.json['nickname']
        profile = UserProfile.find_by_nickname(nickname)
        if profile:
            user = profile.user
            request.json['email'] = user.email
        else:
            return {'status':'error', 'message': 'No user registered with nickname '+nickname}
    except:
        pass

    form = LoginForm(MultiDict(request.json), csrf_enabled=False)

    if not form.validate_on_submit():
        raise ValidationError(form.errors)

    if not login_user(form.user):
        return {"status": "error", "message": "Failed login user"}

    if not profile:
        profile = current_user.profile

    return {
        "user_token":form.user.get_auth_token(),
        "profile":profile.to_dict()
    }

@app.route('/api/password/change', methods=['POST'])
@auth_token_required
@api_response
def change_password():
    form = ChangePasswordForm(MultiDict(request.json), csrf_enabled=False)

    if form.validate_on_submit():
        change_user_password(current_user, form.new_password.data)
        request.json['email'] = current_user.email
        form = LoginForm(MultiDict(request.json), csrf_enabled=False)
        form.validate_on_submit()
        login_user(form.user)
        return {"status": "ok", 'user_token': form.user.get_auth_token()}

    raise ValidationError(form.errors)



@app.route('/api/logout',methods=['GET'])
@auth_token_required
@api_response
def logout():
    if current_user.is_authenticated():
        logout_user()

    return {"status":"ok"}


@app.route('/api/register', methods=['POST'])
@api_response
def register():
    form_data = MultiDict(request.json)
    form = RegisterUserForm(form_data, csrf_enabled=False)


    if UserProfile.check_nickname(form_data['nickname']) is not 0:
        raise ValidationError({"nickname":"Username already exists"})

    if not form.validate_on_submit():
        raise ValidationError(form.errors)


    #after_this_request(lambda: orm.db.session.commit())

    #user = register_user(**form.to_dict())
    data = form.to_dict()

    if 'nickname' in request.json and UserProfile.check_nickname(request.json['nickname']) is not 0:
        return {'status': 'error', 'message': 'User with this nickname is already registered'}

    data['password'] = encrypt_password(data['password'])
    user = user_datastore.create_user(**data)

    #TODO: save user profile data (first name and last name)

    profile = UserProfile(
        first_name = form_data['first_name'],
        last_name = form_data['last_name'],
        nickname=form_data['nickname'],
        allow_linkedin = False)

    profile.user = user

    orm.db.session.add(profile)
    orm.db.session.commit()
    login_user(user)

    account = UserAccount()
    account.amount = 1000
    account.user_id = user.id
    orm.db.session.add(account)
    orm.db.session.commit()

    return {
        "user_token": user.get_auth_token(),
        "profile": profile.to_dict()
    }

@app.route("/api/check_nickname", methods=["POST"])
@api_response
def check_nickname():
    return {"exists": UserProfile.check_nickname(request.json['nickname']) is not 0}


@app.route("/api/restore_password", methods=["POST"])
@api_response
def restore_password():
    form_data = MultiDict(request.json)
    user = User.get_by_email(form_data['email'])
    if not user:
        raise NotFound()
    #GENERATE NEW PASSWORD
    return {"status":"ok"}


@app.route("/api/profile/<int:profile_id>", methods=["GET"])
@api_response
@cached
def show_profile(profile_id):
    profile = UserProfile.query.get(profile_id)

    if not profile:
        raise NotFound()

    is_followed_by_user = False
    if _check_token():
        is_followed_by_user = Follower.user_follow(profile.user_id, current_user.id)

    if profile.extra is not None:
        return {
            "profile":profile.to_dict(),
            'invested_ideas': profile.extra.invested_ideas,
            'kudos_amount': profile.extra.kudos_amount,
            "ideas_count":profile.extra.ideas_count,
            "watching_ideas":profile.extra.watching_ideas,
            "following_count":profile.extra.following_count,
            "followers_count": profile.extra.followers_count,
            "email":profile.extra.email,
            "isFollowingByUser": is_followed_by_user
        }
    else:
        deferred.defer(aggregate_profile_extra, profile_id)

    followers = Follower.get_followers_count(profile.user_id)

    following = Follower.get_following_count(profile.user_id)

    ideas = Watcher.get_user_ideas_count(profile.user_id)



    kudos = 0
    invested_ideas = 0
    if profile.user.account:
        kudos = profile.user.account.amount
        from sqlalchemy import distinct
        invested_ideas = orm.db.session.query(count(distinct(KudosTransaction.idea_account_id)))\
            .filter(KudosTransaction.user_account_id == profile.user.account.id)\
            .scalar()




    return {"profile":profile.to_dict(),
            'invested_ideas': invested_ideas,
            'kudos_amount': kudos,
            "ideas_count":Idea.get_user_ideas_count(profile.user_id),
            "watching_ideas":ideas,
            "following_count":following,
            "followers_count": followers,
            "email":profile.user.email,
            "isFollowingByUser": is_followed_by_user}

@app.route("/api/profile/<int:profile_id>", methods=["POST"])
@auth_token_required
@api_response
def update_profile(profile_id):
    profile = UserProfile.query.get(profile_id)

    if not profile:
        raise NotFound()

    #if 'nickname' in request.json and\
    #                profile.nickname is not request.json['nickname'] and\
    #                UserProfile.check_nickname(request.json['nickname']) is not 0:
    #    return {'status': 'error', 'message': 'User with this nickname is already registered'}


    images = profile.images

    profile.images = []
    orm.db.session.commit()

    imgs = []
    for image in images:
        imgs.append(image)



    ProfileForm = model_form(UserProfile, db_session=orm.db.session, base_class=Form)
    form = ProfileForm(MultiDict(request.json), profile, csrf_enabled=False)
    if not form.validate_on_submit():
        raise ValidationError(form.errors)

    form.populate_obj(profile)

    avatar = False
    try:
        avatar = request.json['image']
    except KeyError:
        pass

    if avatar is not False:
        img = Image()
        img.path = "profiles/"+str(profile.user.id)
        imgs.append(img)
        orm.db.session.add(img)

        save_image_for_profile(avatar, img.path)
        save_image_for_profile(avatar, img.path+"_1", 100, 100)

    orm.db.session.commit()
    profile.images = imgs
    orm.db.session.commit()

    return {"status":"ok"}



### Idea API
@app.route('/api/idea/<int:idea_id>', methods=['GET'])
@api_response
@cached
def idea_get(idea_id):
    idea, comments_count = Idea.get_with_comments_count(idea_id)
    if not idea:
        raise NotFound()

    watchers = orm.db.session.query(count(Watcher.user_id)).filter(Watcher.idea_id == idea_id).scalar()

    comments = Comment.query.filter(Comment.idea_id == idea_id).order_by(Comment.date_created.desc()).limit(3).all()

    res = []
    for c in comments:
        dict = c.to_dict()
        dict['likes_count'] = Like.likes_count_for_entity(entity_class_name="Comment", entity_id=c.id)
        if _check_token():
            dict['user_like'] = Like.user_like(current_user.id, "Comment", c.id)
        else:
            dict['user_like'] = False
        dict['user'] = c.user.profile.to_dict()
        res.append(dict)

    image_ids = []
    for img in idea.images:
        is_thumb = False
        if img.path.endswith("_thumb"):
            is_thumb = True
        image_ids.append({"id":img.id,"is_thumb":is_thumb, 'path': img.path})

    user_watch = False
    if _check_token():
        user_watch = Watcher.user_watch(current_user.id, idea.id)

    videos = []
    for video in idea.videos:
        videos.append(video.to_dict())

    kudos = 0
    if idea.account:
        kudos = idea.account.amount

    investors = get_investors(idea)

    return {
        "comments_count": comments_count,
        "idea":idea.to_dict(),
        "user":idea.user.profile.to_dict(),
        "watchers":watchers,
        "user_watch":user_watch,
        "comments":res,
        'kudos': kudos,
        'idea_rating': RatingHistory.get_for_idea(idea.id),
        "images":image_ids,
        "investors":investors,
        "videos":videos}

@app.route("/api/idea/<int:page>/<int:per_page>/<int:competition_id>", methods=['GET'])
@api_response
@cached
def idea_list(page, per_page, competition_id):

    idea_filter = request.args.get('filter')

    if idea_filter == "comments":
        q = Idea.query.join(Comment).filter(Idea.deleted == False, Idea.competition_id == competition_id).group_by(Idea).order_by(count(Comment.id).desc())
    elif _check_token() and idea_filter == "friends":
        uid = current_user.id
        subq = orm.db.session.query(Follower.follower_id).filter(Follower.user_id == uid).subquery()
        q = Idea.query.outerjoin(Follower, Follower.user_id == Idea.user_id).filter(Idea.deleted == False, Follower.user_id.in_(subq), Follower.follower_id == uid, Idea.competition_id == competition_id)
    elif idea_filter == 'kudos':
        q = Idea.query.join(IdeaAccount)\
            .filter(IdeaAccount.amount > 0, Idea.deleted == False, Idea.competition_id == competition_id)\
            .order_by(IdeaAccount.amount.desc()).group_by(Idea.id)
    else:
        q = Idea.query.filter(Idea.deleted == False, Idea.competition_id == competition_id).order_by(Idea.date_created.desc())

    ideas = q.paginate(page, per_page)
    result = []
    for idea in ideas.items:
        dict = idea.to_dict()
        dict['user'] = idea.user.profile.to_dict()
        dict['comments_count'] = Comment.comments_count(idea.id)
        dict['watchers'] = Watcher.get_idea_watchers_count(idea.id)
        dict['idea_rating'] = RatingHistory.get_for_idea(idea.id)
        if idea.account:
            dict['kudos'] = idea.account.amount

        for image in idea.images:
            if image.path.endswith("_thumb"):
                dict['thumbnail'] = image.id
                dict['thumbnail_path'] = image.path
        result.append(dict)

    return result


@app.route('/api/idea/list/<int:user_id>/<int:page>/<int:per_page>/', methods=['GET'])
@auth_token_required
@api_response
@cached
def current_users_ideas(user_id, page, per_page):
    user = User.query.get(user_id)
    if not user:
        abort(404)

    q = Idea.query.filter(Idea.user_id == user_id, Idea.deleted == False)

    ideas = q.paginate(page, per_page)

    result = []
    for idea in ideas.items:
        dict = idea.to_dict()
        dict['user'] = idea.user.profile.to_dict()
        dict['comments_count'] = Comment.comments_count(idea.id)
        dict['watchers'] = Watcher.get_idea_watchers_count(idea.id)
        dict['idea_rating'] = RatingHistory.get_for_idea(idea.id)
        if idea.account:
            dict['kudos'] = idea.account.amount
        for image in idea.images:
            if image.path.endswith("_thumb"):
                dict['thumbnail'] = image.id
                dict['thumbnail_path'] = image.path
        result.append(dict)

    return result

@app.route('/api/idea/watching/<int:user>/<int:page>/<int:per_page>/', methods=['GET'])
@auth_token_required
@api_response
@cached
def current_user_watched_ideas(user, page, per_page):
    ideas = Watcher.get_user_ideas(user).paginate(page, per_page)

    result = []
    for idea in ideas.items:
        dict = idea.to_dict()
        dict['user'] = idea.user.profile.to_dict()
        dict['idea_rating'] = RatingHistory.get_for_idea(idea.id)
        if idea.account:
            dict['kudos'] = idea.account.amount
        for image in idea.images:
            if image.path.endswith("_thumb"):
                dict['thumbnail'] = image.id
                dict['thumbnail_path'] = image.path
        result.append(dict)

    return result


@app.route('/api/idea', methods=['PUT'])
@auth_token_required
@api_response
def idea_create():
    IdeaForm = model_form(Idea, db_session=orm.db.session, base_class=Form)
    idea = Idea()
    idea.deleted = False
    idea.account = IdeaAccount(amount=0)
    competition = Competition.query.get(request.json['competition_id'])
    if not competition:
        raise NotFound()

    images = request.json['images']
    if not images:
        raise ValidationError({"images":"No images"})

    request.json['images'] = None

    idea_form = IdeaForm(MultiDict(request.json), idea, csrf_enabled=False)

    if not idea_form.validate_on_submit():
        raise ValidationError(idea_form.errors)



    #idea_form.populate_obj(idea)
    idea.title = request.json['title']
    idea.description = request.json['description']
    idea.user_id = current_user.id
    idea.competition = competition
    idea.date_created = datetime.now()
    idea.date_updated = datetime.now()

    orm.db.session.add(idea)
    orm.db.session.commit()

    counter = 0
    for image in images:
        img = Image()
        img.path = "ideas/"+str(idea.id)+"_"+str(counter)
        if "is_thumb" in image and image["is_thumb"] == 'thumb':
            img.path += "_thumb"

        img.ideas = [idea]
        orm.db.session.add(img)
        save_image_for_profile(image['data'], img.path)
        counter += 1


    orm.db.session.commit()

    deferred.defer(track_idea_create, idea.id)
    deferred.defer(build_rating, idea.competition_id)

    return {"id":idea.id}

@app.route('/api/idea/<int:idea_id>', methods=['POST'])
@auth_token_required
@api_response
def idea_update(idea_id):
    IdeaForm = model_form(Idea, db_session=orm.db.session, base_class=Form)
    idea = Idea.get_by_id(idea_id)

    if not idea:
        raise NotFound()

    idea_form = IdeaForm(MultiDict(request.json), idea, csrf_enabled=False)

    if not idea_form.validate_on_submit():
        raise ValidationError(idea_form.errors)

    created_date = idea.date_created
    idea_form.populate_obj(idea)
    idea.date_updated = datetime.now()
    idea.date_created = created_date
    orm.db.session.add(idea)
    orm.db.session.commit()
    return {"status":"ok"}

@app.route("/api/idea/<int:idea_id>", methods=['DELETE'])
@auth_token_required
@api_response
def idea_delete(idea_id):
    idea = Idea.get_by_id(idea_id)
    if not idea:
        raise NotFound()

    idea.deleted = True
    orm.db.session.commit()
    return {"status":"ok"}


@app.route('/api/comment/<int:idea_id>', methods=['POST'])
@auth_token_required
@api_response
def idea_comment(idea_id):
    idea = Idea.get_by_id(idea_id)
    if not idea:
        raise NotFound()

    comment = Comment()
    comment.idea = idea
    CommentForm = model_form(Comment, db_session=orm.db.session, base_class=Form)
    comment_form = CommentForm(MultiDict(request.json), comment, csrf_enabled=False)

    if not comment_form.validate_on_submit():
        raise ValidationError(comment_form.errors)

    comment_form.populate_obj(comment)
    comment.date_created = datetime.now()
    comment.date_updated = datetime.now()
    comment.user_id = current_user.id
    orm.db.session.add(comment)
    orm.db.session.commit()

    deferred.defer(track_comment,comment.id)

    return {"commentId":comment.id}

@app.route('/api/comment/<int:idea_id>/<int:page>/<int:per_page>', methods=['GET'])
@api_response
@cached
def comments_list(idea_id, page = 1, per_page = 5):
    idea = Idea.get_by_id(idea_id)
    if not idea:
        raise NotFound()

    comments = Comment.query.filter(Comment.idea == idea).order_by(Comment.date_created.desc()).paginate(page, per_page)
    result = []
    for comment in comments.items:

        dict = comment.to_dict()
        dict['user'] = comment.user.profile.to_dict()
        dict['likes_count'] = Like.likes_count_for_entity(entity_class_name="Comment", entity_id=comment.id)
        if _check_token():
            dict['user_like'] = Like.user_like(current_user.id, "Comment", comment.id)
        else:
            dict['user_like'] = False

        result.append(dict)

    return result

@app.route('/api/watch/<int:idea_id>', methods=["GET"])
@auth_token_required
@api_response
def watch_idea(idea_id):
    idea = Idea.get_by_id(idea_id)
    if not idea:
        raise NotFound()

    watchers = Watcher.query.filter(Watcher.idea_id == idea_id, Watcher.user_id == current_user.id).all()

    if len(watchers) > 0:
        raise ValidationError({"watcher":"You are already watch this idea"})

    watcher = Watcher()
    watcher.idea_id = idea_id
    watcher.user_id = current_user.id

    orm.db.session.add(watcher)
    orm.db.session.commit()

    deferred.defer(track_watch_idea, watcher.id)
    deferred.defer(aggregate_profile_extra, current_user.profile.id)
    return {"status":"success"}


@app.route('/api/unwatch/<int:idea_id>', methods=["GET"])
@auth_token_required
@api_response
def unwatch_idea(idea_id):
    idea = Idea.get_by_id(idea_id)
    if not idea:
        raise NotFound()

    watchers = Watcher.query.filter(Watcher.idea_id == idea_id, Watcher.user_id == current_user.id).all()

    if len(watchers) < 1:
        raise ValidationError({"watcher":"You are not watch this idea"})

    watcher = watchers[0]
    orm.db.session.delete(watcher)
    orm.db.session.commit()
    deferred.defer(aggregate_profile_extra, current_user.profile.id)
    return {'status':'success'}


#followes
@app.route('/api/follow/<int:user_id>')
@auth_token_required
@api_response
def follow_user(user_id):
    #Mark current user  as follower to user_id
    user = User.query.get(user_id)
    if not user:
        raise NotFound()

    if Follower.user_follow(user_id, current_user.id):
        raise ValidationError({"follower":"Current user already follow user"+str(user_id)})

    follower = Follower()
    follower.user_id = user_id
    follower.follower = current_user
    orm.db.session.commit()


@app.route('/api/unfollow/<int:user_id>')
@auth_token_required
@api_response
def unfollow_user(user_id):
    #Remove mark that current user is follower for user_id
    user = User.query.get(user_id)
    if not user:
        raise NotFound()

    if not Follower.user_follow(user_id, current_user.id):
        raise ValidationError({"follower":"Current user does not follow user"+str(user_id)})

    follower = Follower.query.filter(Follower.user_id == user_id, Follower.follower_id == current_user.id).first()
    orm.db.session.delete(follower)
    orm.db.session.commit()

    return {"status":"success"}

@app.route('/api/followers/<int:user_id>/<int:page>/<int:per_page>', methods=['GET'])
#@auth_token_required
@api_response
@cached
def get_followers(user_id, page, per_page):
    user = User.query.get(user_id)
    if not user:
        raise NotFound()


    followers = Follower.get_followers(user_id=user_id).paginate(page, per_page)

    res = []

    for follower in followers.items:
        f = follower.to_dict()
        profile = follower.follower.profile
        if profile:
            f['profile'] = profile.to_dict()
        f['ideas_count'] = Idea.get_user_ideas_count(follower.follower_id)
        if _check_token():
            is_followed_by_user = Follower.user_follow(profile.user_id, current_user.id)
            f['isFollowingByUser'] = is_followed_by_user

        res.append(f)

    return res

@app.route('/api/following/<int:user_id>/<int:page>/<int:per_page>', methods=['GET'])
#@auth_token_required
@api_response
@cached
def get_following(user_id, page, per_page):
    user = User.query.get(user_id)
    if not user:
        logging.debug('User not found')
        raise NotFound()


    followers = Follower.get_following(user_id=user_id).paginate(page, per_page)

    res = []

    for follower in followers.items:
        f = follower.to_dict()
        profile = follower.user.profile
        if profile:
            f['profile'] = profile.to_dict()
        f['ideas_count'] = Idea.get_user_ideas_count(follower.user_id)
        if _check_token():
            is_followed_by_user = Follower.user_follow(profile.user_id, current_user.id)
            f['isFollowingByUser'] = is_followed_by_user

        res.append(f)

    return res

#likes
@app.route('/api/like/idea/<int:idea_id>')
@auth_token_required
@api_response
def like_idea(idea_id):
    likes = Like.like_entity(idea_id,"Idea")
    return {"likes_count":likes}


@app.route('/api/like/comment/<int:comment_id>')
@auth_token_required
@api_response
def like_comment(comment_id):
    likes = Like.like_entity(comment_id,"Comment")
    return {"likes_count":likes}


@app.route('/api/like/image/<int:image_id>')
@auth_token_required
@api_response
def like_image(image_id):
    likes = Like.like_entity(image_id,"Image")
    return {"likes_count":likes}


#users feed
@app.route('/api/feed/<int:page>/<int:per_page>', methods=['GET'])
@auth_token_required
@api_response
def get_feed(page, per_page):
    uid = current_user.id
    feed = UserFeed.query.filter(UserFeed.user_id == uid).order_by(UserFeed.date_created.desc()).paginate(page, per_page)

    res = []
    for item in feed.items:
        i = item.to_dict()
        i['data'] = loads(i['data'])
        res.append(i)

    return res

@app.route('/api/feed/friends/<int:page>/<int:per_page>', methods=['GET'])
@auth_token_required
@api_response
def get_friends_activity(page, per_page):
    uid = current_user.id

    friend_ids = Follower.get_friend_ids(uid)

    #if len(friend_ids) > 0:
    feed = UserFeed.query.filter(UserFeed.user_id == uid, UserFeed.action_performer.in_(Follower.get_friend_ids(uid))).paginate(page, per_page)
    #else:
    #    return []

    res = []
    for item in feed.items:
        i = item.to_dict()
        i['data'] = loads(i['data'])
        res.append(i)

    return res

@app.route('/api/feed/ideas/<int:page>/<int:per_page>', methods=['GET'])
@auth_token_required
@api_response
def get_user_ideas_activity(page, per_page):
    uid = current_user.id

    ideas_ids = Idea.get_user_ideas_ids(uid)
    #if len(ideas_ids) > 0:
    feed = UserFeed.query.filter(UserFeed.entity_id.in_(ideas_ids), UserFeed.entity_class=="Idea", UserFeed.user_id == uid).paginate(page, per_page)
    #else:
    #    return []
    res = []
    for item in feed.items:
        i = item.to_dict()
        i['data'] = loads(i['data'])
        res.append(i)

    return res


@app.route('/api/feed/watched/<int:page>/<int:per_page>', methods=['GET'])
@auth_token_required
@api_response
def get_user_watched_ideas_activity(page, per_page):
    uid = current_user.id

    ideas_ids = Watcher.get_user_ideas_ids(uid)
    feed = UserFeed.query.filter(UserFeed.entity_id.in_(ideas_ids), UserFeed.entity_class=="Idea", UserFeed.user_id == uid).paginate(page, per_page)

    res = []
    for item in feed.items:
        i = item.to_dict()
        i['data'] = loads(i['data'])
        res.append(i)

    return res

#linkedin
@app.route('/api/linkedin',methods=['POST'])
@auth_token_required
@api_response
def set_linkedin_id():
    profile = UserProfile.query.filter(UserProfile.user_id == current_user.id).first()
    profile.linkedin_id = request.json['linkedin_id']

    try:
        profile.allow_linkedin = request.json['allow_linkedin']
    except:
        pass

    orm.db.session.commit()


#competitions
@app.route("/api/competitions")
@api_response
@cached
def competition_list():
    competitions = Competition.query.all()
    result = []
    for competition in competitions:
        result.append(competition.to_dict())

    return result


@app.route("/api/competition/ideas/<int:competition_id>/<int:page>/<int:per_page>")
@api_response
@cached
def competition_ideas(competition_id, page, per_page):
    competition = Competition.query.get(competition_id)
    if not competition:
        raise NotFound()

    ideas = Idea.query.filter(Idea.competition_id == competition_id, Idea.deleted == False).order_by(Idea.date_created.desc())\
        .paginate(page, per_page)

    result = []
    for idea in ideas.items:
        dict = idea.to_dict()
        dict['user'] = idea.user.profile.to_dict()
        dict['idea_rating'] = RatingHistory.get_for_idea(idea.id)
        for image in idea.images:
            if image.path.endswith("_thumb"):
                dict['thumbnail'] = image.id
                dict['thumbnail_path'] = image.path
        result.append(dict)

    return result

@app.route('/api/competition/<int:comp_id>')
@api_response
def competition_info(comp_id):
    competition = Competition.query.get(comp_id)
    if not competition:
        raise NotFound()

    coaches = User.query.filter(User.is_coach == True).limit(2).all()
    coaches_count = orm.db.session.query(count(User.id)).filter(User.is_coach == True).scalar()
    res = competition.to_dict()
    res['coaches'] = []
    res['ideas_count'] = len(competition.ideas)
    for coach in coaches:
        res['coaches'].append(coach.profile.to_dict())

    res['images'] = []
    for image in competition.images:
        res['images'].append(image.to_dict())

    res['coaches_count'] = coaches_count

    res['video'] = {"url": "http://cdn.ideasbritain.com/Ideas_Britain_Text_Promo_Update low.m4v", "preview": "http://cdn.ideasbritain.com/4channels.jpg"}
    return res


#Investments
@app.route('/api/invest/<int:idea_id>/<int:amount>')
@auth_token_required
@api_response
def invest(idea_id, amount):
    user = current_user
    idea = Idea.get_by_id(idea_id)
    if not idea:
        abort(404)

    if user.account.amount < amount:
        return {'error': 'not enough kudos'}

    ua = orm.db.session.query(UserAccount).filter(UserAccount.user_id == current_user.id).first()
    ia = orm.db.session.query(IdeaAccount).filter(IdeaAccount.idea_id == idea.id).first()


    t = KudosTransaction()
    t.amount = amount
    t.user_account_id = ua.id
    t.idea_account_id = ia.id

    ua.amount = user.account.amount - amount
    ia.amount = idea.account.amount + amount

    orm.db.session.add(t)
    orm.db.session.commit()
    deferred.defer(build_rating, idea.competition_id)
    deferred.defer(aggregate_profile_extra, current_user.profile.id)
    deferred.defer(aggregate_profile_extra, idea.user.profile.id)
    return {'status': 'ok'}


@app.route('/api/user/account')
@auth_token_required
@api_response
def get_kudos_amount():
    account = current_user.account
    return account.to_dict()

@app.route('/api/ideas/invested/<int:page>/<int:per_page>')
@auth_token_required
@api_response
def get_invested_ideas(page, per_page):
    #import pdb; pdb.set_trace()
    #account_ids = orm.db.session.query(KudosTransaction.idea_account_id)\
    #    .filter(KudosTransaction.user_account == current_user.account)\
    #    .group_by(KudosTransaction.idea_account_id)

   #ideas = Idea.query.join(IdeaAccount).filter(IdeaAccount.id.in_(account_ids))\
   #     .all()
        #.limit(per_page).offset((page - 1) * per_page).all()

    transactions = KudosTransaction.query.join(UserAccount).join(IdeaAccount)\
        .filter(UserAccount.user_id == current_user.id).order_by(KudosTransaction.date_created.desc())\
        .paginate(page, per_page)
    result = []

    #import pdb; pdb.set_trace()
    for t in transactions.items:
        if t.idea_account.idea.deleted:
            continue

        dict = t.to_dict()
        dict['user'] = t.idea_account.idea.user.profile.to_dict()
        dict['idea'] = t.idea_account.idea.to_dict()
        dict['kudos_amount'] = t.idea_account.amount
        dict['idea_rating'] = t.idea_account.idea.ratings[0].to_dict()
        for image in t.idea_account.idea.images:
            if image.path.endswith("_thumb"):
                dict['thumbnail'] = image.id
                dict['thumbnail_path'] = image.path
        result.append(dict)

    return result


@app.route('/api/ideas/portfolio/<int:page>/<int:per_page>')
@auth_token_required
@api_response
def get_portfolio_ideas(page, per_page):
    transactions = KudosTransaction.query.join(UserAccount).join(IdeaAccount)\
        .filter(UserAccount.user_id == current_user.id).order_by(IdeaAccount.amount.desc()).group_by(IdeaAccount.amount)\
        .paginate(page, per_page)

    result = []

    #import pdb; pdb.set_trace()
    for t in transactions.items:
        if t.idea_account.idea.deleted:
            continue

        dict = {}
        dict['user'] = t.idea_account.idea.user.profile.to_dict()
        dict['idea'] = t.idea_account.idea.to_dict()
        dict['kudos_amount'] = t.idea_account.amount
        dict['idea_rating'] = t.idea_account.idea.ratings[0].to_dict()
        for image in t.idea_account.idea.images:
            if image.path.endswith("_thumb"):
                dict['thumbnail'] = image.id
                dict['thumbnail_path'] = image.path
        result.append(dict)

    return result

@app.route('/api/investors/<int:idea_id>/<int:page>/<int:per_page>')
@api_response
def investors_get(idea_id, page, per_page):
    idea = Idea.query.get(idea_id)
    if not idea:
        abort(404)

    return get_investors(idea, page, per_page)


def get_investors(idea, page=1 , per_page=3):
    transactions = KudosTransaction.query\
        .filter(KudosTransaction.idea_account_id == idea.account.id)\
        .order_by(KudosTransaction.amount.desc()).paginate(page, per_page)

    result = []
    for transaction in transactions.items:
        profile = {}
        if transaction.user_account.user.profile:
            profile = transaction.user_account.user.profile.to_dict()

        result.append({'amount': transaction.amount, 'user': profile})

    return result

def image_response(data, size):
    return app.response_class(data, mimetype="image/png", headers={"Content-Length":size})


@app.route("/image/profile/<int:image_id>/<int:is_thumb>")
def display_profile_image(image_id, is_thumb = 0):
    image = Image.query.get(image_id)
    if not image:
        abort(404)

    try:
        if is_thumb is not 0:
            data = get_image_data(image.path+"_1")
        else:
            data = get_image_data(image.path)

    except cloudstorage.NotFoundError:
        abort(404)

    return image_response(data['data'],data['size'])




@app.route("/image/<int:img_id>")
def display_image(img_id):
    image = Image.query.get(img_id)
    if not image:
        abort(404)

    try:
        data = get_image_data(image.path)
    except cloudstorage.NotFoundError:
        abort(404)

    return image_response(data['data'],data['size'])



def get_image_data(path):
    bucket = "/" + app.config['BUCKET_NAME']
    write_retry_params = cloudstorage.RetryParams(backoff_factor=1.1)
    gcs_file = cloudstorage.open(bucket+"/" + path,
                        'r',
                        retry_params=write_retry_params)

    return {"data": gcs_file.read(), "size": cloudstorage.stat(bucket+"/" + path).st_size}

def save_image_for_profile(b64image, path, width = -1, height = -1):
    from google.appengine.api import images
    img = images.Image(base64.b64decode(b64image))
    if width is -1:
        width = img.width
    if height is -1:
        height = img.height

    img.resize(width, height)
    data = img.execute_transforms(output_encoding=images.PNG)
    create_file(file_name=path, data=data)


def competition_add_idea(competition_id, idea_id):
    competition = Competition.query.get(competition_id)
    idea = Idea.get_by_id(idea_id)
    if not competition or not idea:
        raise NotFound()


@app.route('/api/idea/video/<int:idea_id>', methods=['POST'])
@auth_token_required
@api_response
def idea_add_video(idea_id):
    idea = Idea.get_by_id(idea_id)
    if not idea:
        abort(404)

    uid = current_user.id
    yid = False
    try:
        yid = request.json['youtube_id']
    except:
        pass

    video = Video()
    video.user_id = uid
    video.idea = idea
    if yid:
        video.youtube_id = yid

    orm.db.session.add(video)
    orm.db.session.commit()

    if not yid:
        upload_url = create_upload_url('/api/video/upload/' + str(video.id))
        return {"upload_url": upload_url}
    else:
        return {"status": "ok"}


@app.route('/api/video/upload/<int:video_id>', methods=['POST'])
@auth_token_required
@api_response
def upload_video(video_id):
    video = Video.query.get(video_id)
    if not video:
        abort(404)

    if request.method == 'POST':
        f = request.files['file']
        header = f.headers['Content-Type']
        parsed_header = parse_options_header(header)
        blob_key = parsed_header[1]['blob-key']
        video.blob_key = blob_key
        orm.db.session.commit()
        return {"status": "ok"}

    return {"status": "fail"}


@app.route('/api/video/<int:video_id>', methods=['GET'])
def get_video(video_id):
    video = Video.query.get(video_id)
    if not video:
        abort(404)

    blob_info = blobstore.BlobInfo.get(video.blob_key)
    response = make_response(blob_info.open().read())
    response.headers['Content-Type'] = blob_info.content_type
    return response


@app.route('/api/video/promo', methods=['GET'])
@api_response
def promo_video():
    cdn = "http://cdn.ideasbritain.com/"
    return {"videos": [
        {"url": "http://cdn.ideasbritain.com/Ideas_Britain_Text_Promo_Update low.m4v", "preview": "http://cdn.ideasbritain.com/4channels.jpg"},
        {"url": "http://cdn.ideasbritain.com/Ideas_Britain_Promo low.m4v", "preview": "http://cdn.ideasbritain.com/ib_1.jpg"},
        {"url": "http://cdn.ideasbritain.com/IB  App Promo (Online) low.m4v", "preview": "http://cdn.ideasbritain.com/ib_2.jpg"}
    ]}

@app.route('/init_db')
def init_db():
    from model.entity import create_tables
    create_tables()
    #security.datastore.create_user(email='test@test.sa',password='test')
    #competition = Competition()
    #competition.title = "lets shake up music"
    #competition.description = "Some awesome competition description"
    #orm.db.session.add(competition)
    #orm.db.session.commit()
    return "Ok"




@app.route('/test_gcs')
def create_file(file_name="test_file_name", data=None, content_type="image/png"):
    bucket = "/" + app.config['BUCKET_NAME']
    write_retry_params = cloudstorage.RetryParams(backoff_factor=1.1)
    gcs_file = cloudstorage.open(bucket+"/"+file_name,
                        'w',
                        content_type=content_type,
                        options={'x-goog-acl': 'public-read'},
                        retry_params=write_retry_params)
    #imgdata = """iVBORw0KGgoAAAANSUhEUgAAAS0AAABlCAYAAAAYn7VbAAAAAXNSR0IArs4c6QAAAAlwSFlzAAAWJQAAFiUBSVIk8AAAABxpRE9UAAAAAgAAAAAAAAAzAAAAKAAAADMAAAAyAAAIuwlSsOsAAAiHSURBVHgB7FlLbiRFEJ0F4iOx8BF6DwsfoY7gA4BUR+gb5BE8C4TY1RF8AUSuYNsrYFlH8IYFSKDhPU/ldGQowm6Dp6vb/UIKZWbki8jIqJcxuHnz7t27N06/wvot9Hfon1CJKqAKqALHrAD7zm9Q9qGvoV2PsotPsfkd9B+oRBVQBVSBU6gA+9EP0C+gD/2qNS02rB+hElVAFVAFTrECPyEp9qk3rWl9f4pZKidVQBVQBUwF2Kcemhb/ZvR/Ev4M2zfQzwmSqgbigDhwRA6w77D/sA9Z+RuLr9mQ+GOXFQIf/jMMoxqWaiAOiANrceAT9KBfbHPC/C2T4f8ltMIOt1aSOle1FwfEAcuBb21zwvxXbv7ljJ9hbZ00Vz3EAXFgLQ586frTH0zEy1rJ6Vw9DHFAHIg40PUoArxETrKJTOKAOLAWB7oexSS8rJWYztWjEAfEgYgDXY8iwEvkJJvIJA6IA2txoOtRTMLLWonpXD0KcUAciDjQ9SgCvEROsolM4oA4sBYHuh7FJLyslZjO1aMQB8SBiANdjyLAS+Qkm8gkDogDa3Gg61FMwstaielcPQpxQByIOND1KAK8RE6yiUzigDiwFge6HsUkvKyVmM7VoxAHxIGIA12PIsBL5CSbyCQOiANrcaDrUUzCy1qJ6Vw9CnFAHIg40PUoArxETrKJTOKAOLAWB7oexSS8rJWYztWjEAfEgYgDXY8iwEvkJJvIJA6IA2txoOtRTMLLWonpXD0KcUAciDjQ9SgCvEROsolM4oA4sBYHuh7FJLyslZjO1aMQB8SBiANdjyLAS+Qkm8gkDogDa3Gg61FMwstaielcPQpxQByIOND1KAK8RE6yiUzigDiwFge6HsUkvKyVmM7Vo3iKA7cg62wIWzG/gT7lp/3zrpH55O8v0hlEAD2AE+XAtSfqsq4nmq8a5cs1yu7Ts7BeVOyXK7Zq+XK1HDxRl3XFqDq/7hp0n54f24sI8LoJcK7fd/BEXdYV47neSXkf9u26T8+ieVEhDyuk6nTcOunPw+PW+5T43fUoNa3LJcIpkfLQXPhD/M4wuGKuH+JfP4fNJ39/2c6AxaEEEk61EgfEgWNwoOtRPNDLMZLQGSK7OCAOHMqBrkfRycuhgYQT6cQBceAYHOh6FA/0cowkLv0M/qg8QsuiXLeaDJjfQe+hFI5c094wj43EbaFl0SuMxHOcoC0uYz4WhzndQivUyowFfUdo5n+zYCrGpozVcon8eN4ErUa3mFss13afedDPYqL5FTAjlPgZaqViwdw20MiXNp4xQavRgnmGH7DHs+qiT+VZnoHNznzNdpRnL7yol9d8+VO4284XHOsKZW4T9DGZsJndYYO9+8CZD52PLtobYffx+MAr9BCZAWJsH2OCLZLovObLhx1J2+c5kVQYGyYaR+xHd49i3SaxSgSGLbo7c9gF+ApblN/wDGzkfwm2rkS8sJdLKMKad4wIzW9Q/IdI1hPsUf6bBF9hf+xMG4uP8D6Jk5mJ9493TMB3sNvz2vzqAPyQYGoSk7En6HNlgkPLq403SZBtgM3uwhAtnh0LN5wUrC3m0uddeVgML5deoI99/8kX/D+s+YiiPJ8bysbhY5ufG2DBsyn6fLLmx3M8dkzOpb1hhwRTDaZhOW4T/CFmW5cWM/KrMLb9NtI3kwEbDdfGGoAjXMNf4tiViAXw8hqLMuCSVte8Y/EFD9ZsAo/JjM3oDk/5+Zi2gdz6TbNmA6qLGnM3HbGyOU3d7n7hcfS52293M5vf0O3sFxVTey7nGyhzzmSHjQqdoZHMMPqYNQIGuNsER3OB2ri8nxfmbTGauwqxIF5eQ5GucSmSh+TMhHvEEHvMOxeclwlz2kBbPswvkyjvmoFh594NdICOy7ydc4X1PTSSCUbuNyzPjYS5NwxHnhXJHYwWx9iReNwQgWCrUBuP8wKNhHdkHIvnOZEwf4vbRiDYfLya4Gjmno0Z1cjf2+Ivdc7afRAWwcs5F2bAZaq/0AFr+tD3GHcvOCeSGUY+YJ9DhS2SAuOh2CnAWt/o8cDloZFFOTFeJBsYbVw2iUhszOzsEY421hAFgq06HH120EgKjDYm55sICNsEtdjrBFccLoF9MNuYtx+s+8kWU4vRfF+bhxkL4uUci8RHwH+hIqkw8mHwXhwfE8ZgrI9Zg5IkQHt07pjga4CnLZIBxih2s5XICTY+qoaxY1bHweEnrCNp34MxpwBwD5s9j3PGjqTC6LERjrbs2+4Chwqbj8u8vFQYGi7L0fpcG3x0rt1vcS99tPV7KHZnMAU9l0LxI8/LJUiqusw5TFDeYwMt0DtoJBXGRsgZ849JnIL4kRQYo5ozl0hmGD2+RkDYhgBrfTO/svjS3+oW60gKjDbuTQSCbTK4VncLtfstHs+PpMLYMBwz3Lzscd/rDrZIGM/qFIEMpiT71szaMeaVNS5z1sKep/n7enSlYlG8nFOhrpF8Iz2Jt4FOUMoM5V1G6FMyAUDfRl7GZGz6v7QWxIykwJidFeFp8/iaAIcAa30zvyRcai7YsXE5b9/HOs0LjnlFcgOjj5Nhq8NmuOicp2w+hzFx4JnEVrc/uzWXd1Bio1htj/vSfQ1Qjr2wMF7OpVi2YU24xBWUuc9QyhZKW/RouG9lxoK+xE9QCv2uoS9dj4KYkRQYs7MiPG0eXxPgEGCtb+aXhEvNrLmNy/mUoDewl2CPdfcxuB4CLE0VavEZjtjnSJTHJglQYGcOXkZvwLrFnYK9qH6Me+nalSoq9DkUiM1lt9zkzn3UdsEBE+qhYu/NmBSewbPs3v+dF8SLpMCYxY7wtHl8TYBDgLW+mV8SLjRntboJ0e9/X2x1tpAJC5tbm/MOkVQYG4Zjhot8H7ONLm47Yw6cePfo3A3sNcBfwzYn9naOxv137Ur1LwAAAP//nxr5+AAAB0tJREFU7VpBjiNFEDQIgYQ4zBP8AZDFC/oJvsOhnzA/qCdYe0Nc+gnzhAYJzr4hJA79hLnAAQkYIna7vNXpLLdrxjvrakdKqarKyszKCkfnlrSzejqWFUzXrrux7D3GO1PvuPX0gAn1XEnvzJzMTdlB072XzgOTOhJg83KzlpxY/z7j2MBufdN1Lo4YcG9OibP9HdL8j9i3Qlw9+xb2NDbOG5tgXPfGP+fHs+h7jrbwi+fakXV7Yu0DnBgboFasL/dZnz1L63eYTPAjKFZqACrWvMHE1ruPmwUjY2we5o5i916yDjGpGWn38jbGLy57x582T5jDyx1t3kfEPPczcTF+buyYzAg/Uiu05XI11nlc9ybmLuN3KnfuTM++zeS35g4Gxnt1D7BbeYDBO082gxQBsVIDSMNYdMBo6+WHVirexxnGJANGe8ZL1jHvmP4w0O7l3R08ppPO8e+nLodV4/imZ7UHz+nkUh/SZpo2u+qwk9aVzptMVO/EDBlf1pHmfM78LpPbmtvkLLvnrT0OPqe+JcZM8OIFrdRw6SYpeot5WjNJNST7c1P6MibNwZxReFa699J5iInNSLvNzbr4QvCkhdH6954jbI3jm8auM3E0z8Vy3+KX5o7zgclmhLhHfzs2mdjeiekKfNNz1ojbOPlSH8576Jykefo5Z+yn/va8W19P4CMYVmoBKIyF86O2PzjXuY99DHs7zMXu4HVpPEJaQDIfMG+hrP0OuoUOUE9YN31sbb3nDFvj+NrYh0wszcSBOXjmZpzTNkApe6jNZ9f0PyWP2LQx6brJBPdOHGvMCWttofThfZiX6wdolC0mqxMaomNmtHe5z/hFs/U/dfYt7kWc3o4EwEpNoHRj8fzRW2haO0m5H/e9gXv0SWOYg7koHTTdu9Q8IO9LJSCBV0+fSdxk/NMcxOIlEhCc5rPzufy7mXjewZMeRnsW1w+e85k2cuAO6uWlbe4uPDuNLfVPYzUHmKkQECu1gdQlFyDxLdm2sNGnH5Vz2tJ7MoaxUTpM0v1LzkM85JnjHnH2jrG+PpOzgT36nBrbTPw55v6MM4YTiTYz8byDJz2M3p2IEbF6rvA8L2+0PZ5IfO/ElvrHczQaoAmIlRpBIkmikBwBStLO3YU+9E0J5RFuLk/JPs97rrDODTR3Xp9J3JyIsbnaTI458+6MM+jjyQCjrcOuGy8Qtv5ELH/fPbRUiPMaamtI1w8nknq/Ual/etatzydQEwwrtQJEovTmMiRKgG6hzaicB6glUQ+bR7ZL48GzPelhHLyN0dZhvIOeqoc+Vh5hmIuzOdeI6WyizJo4ElObw1sTX08CjJ5/auMdeBcrHQypnzdv4TNA52SAww56Dl7M6Qlr9Gq495xhGzL+Xo5btU2gIwhWagemwYX4IZ0r9GXMa907ZAqjnTVsoZxHbTFfQ8+tr4Fvqud8gLncjGWu4CjtubhT9jXiGBt1U5An1hNjOa4KlGe10GCUtjW0JBd9ma8xyhpzeUr9c3luzQ5I3wsvb2UpgJA8W2iA9kZp4x59Xvu+PNuTAONr16LzhHkNHJh8LyzYSg2XqLnGYAEf17TXfC/Vrt/vQ3Fg8snwECsf6mDlfUfqYAEf17QLI2EgDhxzYPLJECArAu0YtEtiEizg45r2S56jXMJzKRyYfDK8lJWlXPRa7xEs4OOa9mutWXXpt/mYHJh8MizEyscs7hbODhbwcU37Ldxfd9TvXMqBySfDYCulCeVfRsLWAj6u7zEKS2EgDhxzYPLJECArAu0YtEtjEv8UIwB8KteXPkP5hOlSOIDP473wUlaWclHdQx+tOLAMDkx6lJrWMn5UfZz6HZfMATUtILDkH1h30++7NA6oaalpqWmLA1VxQE1LhK2KsEt7Neg+5S9hNS01LTUtcaAqDqhpibBVEVYvk/KXydIwU9NS01LTEgeq4oCalghbFWGX9mrQfcpfjmpaalpqWuJAVRxQ0xJhqyKsXiblL5OlYaampaalpiUOVMUBNS0RtirCLu3VoPuUvxzVtNS01LTEgao4oKYlwlZFWL1Myl8mS8NMTUtNS01LHKiKA2paImxVhF3aq0H3KX85qmmpaalpiQNVcUBNS4StirB6mZS/TJaGmZqWmpaaljhQFQfUtETYqgi7tFeD7lP+clTTUtNS0xIHquKAmpYIWxVh9TIpf5ksDTM1LTUtNS1xoCoOHDWtvyeWp6cv9INW9YMu7V9V3Ucvq5QDX5n+9Cc3fzfG79W01LTEAXHgSjjAfpTKb2xab1IL5r9CP4Wm3U5z4SEOiAOvzYFPxn6E4SBvWMTX0H8PpneTXzB8B/0M+tqF6jxhLg7cNge+HPsP+1Aq7FPfRHL8kO5oLgSEgBC4QgTYp1axaX2OxU9XWKRKEgJCQAgQgZ+h7FOHpsXmxf81/BH6H1QiBISAELgGBNiP2JcOf9UQX1rp+C0c+Az7A/oPVCIEhIAQeE0E/sJh/KsG9iH2o7Q/rf4HbV7hEewkCkcAAAAASUVORK5CYII="""
    #data = save_image_for_profile(imgdata)
    gcs_file.write(data)
    gcs_file.close()

    return "ok"


@app.route('/generate_feeds')
def create_feeds():
    generate_feeds()
    return "ok"


@app.route('/generate_accounts')
def generate_accounts():
    for idea in Idea.query.all():
        if not idea.account:
            account = IdeaAccount()
            account.amount = 0
            account.idea_id = idea.id
            orm.db.session.add(account)

    for user in User.query.all():
        if not user.account:
            account = UserAccount()
            account.amount = 1000
            account.user_id = user.id
            orm.db.session.add(account)

    orm.db.session.commit()
    return "ok"

@app.route('/tests')
def tests_runner():
    from tests_runner import run
    run()

@app.route('/build_ratings')
def ratings_build():
    for competition in Competition.query.all():
        build_rating(competition.id)


@app.route('/move_images')
def move_images():
    new_b_name = '/' + app.config['BUCKET_NAME']
    b_name = '/idbr-profile-images'
    
    for image in Image.query.order_by(Image.id.desc()).all():
        deferred.defer(move_image, image.path)

def move_image(path):
    new_b_name = '/' + app.config['BUCKET_NAME']
    b_name = '/idbr-profile-images'

    try:
        write_retry_params = cloudstorage.RetryParams(backoff_factor=1.1)
        old_f = cloudstorage.open(b_name+'/'+path)
        new_f = cloudstorage.open(new_b_name+'/'+path,
                                'w',
                                content_type='image/jpeg',
                                options = {'x-goog-acl': 'public-read'},
                                retry_params=write_retry_params)

        new_f.write(old_f.read())
        old_f.close()
        new_f.close()
    except NotFoundError:
        return False

@app.route('/aggregate_profile_extra')
def aggregate_profiles():
    profiles = UserProfile.query.all()

    for profile in profiles:
        deferred.defer(aggregate_profile_extra, profile.id)
