__author__ = 'kozloffsky@gmal.com'

import unittest
from google.appengine.ext import testbed


class BaseTestCase(unittest.TestCase):

    def setUp(self):
        from ideasbritain import app, db
        self.app = app
        self.app.config['TESTING'] = True
        self.client = app.test_client()

        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_all_stubs()


class FakeTestCase(BaseTestCase):

    def testSomeThing(self):
        self.assertEqual(1,2,'Fial')