import os
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


def init_sqlalchemy():
    """Create new instance of SQL Alchemy engine according to environment
    """
    #if (os.getenv('SERVER_SOFTWARE') and
    #    os.getenv('SERVER_SOFTWARE').startswith('Google App Engine/')):
    engine = create_engine('mysql+gaerdbms:///ibdb',
                               connect_args={"instance":"ideas-britan"})
    #else:
    #    engine = create_engine('mysql://root@localhost/ibdb')
    return engine

class orm():
    Base = None
    db = None

    def __init__(self, app):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+gaerdbms:///ibdb?instance=ideas-britain:ibdb'
        orm.db = SQLAlchemy(app)
        orm.Base = self.db.Model

class Serializable(object):
    __public__ = None

    def to_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

