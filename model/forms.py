from flask.ext.security.forms import RegisterForm, get_form_field_label
from flask.ext.wtf.form import Form
from wtforms.fields.core import StringField
from wtforms.validators import DataRequired, Length, Regexp

class ProfileFormMixin():
    first_name = StringField(get_form_field_label('first_name'),
                             validators=[DataRequired(message='First Name not provided')])
    last_name = StringField(get_form_field_label('last_name'),
                             validators=[DataRequired(message='Last Name not provided')])
    nickname = StringField(get_form_field_label('nickname'),
                             validators=[
                                 DataRequired(message='Nickname not provided'),
                                 Length(min=4),
                                 Regexp("[a-z]\w")
                             ])

class IdeaFormMixin():
    title = StringField(get_form_field_label('title'),
                             validators=[
                                 DataRequired(message='Title not provided'),
                                 Length(min=4),
                                 Regexp("[a-z]\w")
                             ])
    description = StringField(get_form_field_label('description'),
                             validators=[DataRequired(message='Description not provided')])
    user_id = StringField(get_form_field_label('description'),
                             validators=[DataRequired(message='Description not provided')])

class RegisterUserForm(RegisterForm, ProfileFormMixin):
    pass


