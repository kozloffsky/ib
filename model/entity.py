from datetime import datetime
from flask.ext.login import current_user
from flask.ext.security import UserMixin, RoleMixin
from flask.json import dumps
from model import orm, Serializable
from sqlalchemy.orm import joinedload
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import Column, Table, ForeignKey
from sqlalchemy.sql.sqltypes import Integer, String, DateTime, Boolean, Text, UnicodeText


#Behaviours
from werkzeug.exceptions import NotFound


class Timestampable():
    date_created = Column(DateTime(), nullable=True, default=datetime.now, onupdate=datetime.now)
    date_updated = Column(DateTime(), nullable=True, default=datetime.now, onupdate=datetime.now)


# Define models
roles_users = orm.db.Table('roles_users',
        orm.db.Column('user_id', orm.db.Integer(), orm.db.ForeignKey('user.id')),
        orm.db.Column('role_id', orm.db.Integer(), orm.db.ForeignKey('role.id')))


class Role(orm.Base, RoleMixin):
    id = orm.db.Column(orm.db.Integer(), primary_key=True)
    name = orm.db.Column(orm.db.String(80), unique=True)
    description = orm.db.Column(orm.db.String(255))

class User(orm.Base, Serializable, Timestampable, UserMixin):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    email = orm.db.Column(orm.db.String(length=80), unique=True)
    password = orm.db.Column(orm.db.String(length=254))
    confirmed = orm.db.Column(orm.db.Boolean(), default=False)
    confirmed_at = orm.db.Column(orm.db.DateTime())
    active = orm.db.Column(orm.db.Boolean())
    is_coach = orm.db.Column(orm.db.Boolean(), default=False)
    account = orm.db.relationship('UserAccount',
                                 uselist=False, backref='user')

    roles = orm.db.relationship('Role', secondary=roles_users,
                            backref=orm.db.backref('users', lazy='dynamic'))

    def __unicode__(self):
        return self.email


    @staticmethod
    def get_by_id(id):
        return orm.db.session.query(User).filter(User.id == id).first()

    @staticmethod
    def get_by_email(email):
        return orm.db.session.query(User).filter(User.email == email).first()


class UserProfile(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    user_id = orm.db.Column(orm.db.ForeignKey('user.id'))
    user = orm.db.relationship("User", backref=orm.db.backref('profile', uselist=False))
    first_name = orm.db.Column(orm.db.String(length=254))
    last_name = orm.db.Column(orm.db.String(length=254))
    nickname = orm.db.Column(orm.db.String(length=123), unique=True)
    description = orm.db.Column(Text())
    linkedin_id  = orm.db.Column(orm.db.String(254))
    allow_linkedin = orm.db.Column(orm.db.Boolean())

    def __unicode__(self):
        return self.first_name + " " + self.last_name


    def to_dict(self):
        dict = Serializable.to_dict(self)
        images = self.images
        if images:
            try:
                dict['image_id'] = self.images[0].id
                dict['image_path'] = self.images[0].path
            except Exception:
                pass

        return dict


    @staticmethod
    def find_by_nickname(nickname):
        return orm.db.session.query(UserProfile).filter(UserProfile.nickname == nickname).options(joinedload('user')).first()

    @staticmethod
    def check_nickname(nickname):
        return orm.db.session.query(func.count(UserProfile.nickname)).filter(UserProfile.nickname == nickname).scalar()

class Idea(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    title = orm.db.Column(orm.db.String(length=254))
    description = orm.db.Column(Text())
    user_id = orm.db.Column(orm.db.ForeignKey('user.id'))
    user = orm.db.relationship("User", backref=orm.db.backref('ideas', order_by=id))
    competition_id = orm.db.Column(orm.db.ForeignKey('competition.id'))
    competition = orm.db.relationship("Competition", backref=orm.db.backref('ideas', order_by=id))
    deleted = orm.db.Column(orm.db.Boolean())
    account = orm.db.relationship('IdeaAccount',
                                  uselist=False, backref='idea')

    def __unicode__(self):
        return self.title

    def comments_count(self):
        return orm.db.session.query(func.count(Comment.id)).filter(Comment.idea_id == self.id).scalar()

    @staticmethod
    def get_with_comments(id):
        return orm.db.session.query(Idea).filter(Idea.id == id, Idea.deleted == False).options(joinedload('comments')).first()

    @staticmethod
    def get_with_comments_count(id):
        return orm.db.session.query(Idea, func.count(Comment.id)).filter(Idea.id == id, Idea.deleted == False).outerjoin(Comment).group_by(Idea).options(joinedload('user')).first()

    @staticmethod
    def get_user_ideas_count(user_id):
        return orm.db.session.query(func.count(Idea.id)).filter(Idea.user_id == user_id, Idea.deleted == False).scalar()

    @staticmethod
    def get_user_ideas_ids(user_id):
        return orm.db.session.query(Idea.id).filter(Idea.user_id == user_id, Idea.deleted == False)

    @staticmethod
    def get_by_id(idea_id):
        return orm.db.session.query(Idea).filter(Idea.id == idea_id, Idea.deleted == False).first()

class Comment(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    user_id = orm.db.Column(orm.db.ForeignKey('user.id'))
    idea_id = orm.db.Column(orm.db.ForeignKey('idea.id'))
    user = orm.db.relationship("User", backref=orm.db.backref('commented_ideas', order_by=id))
    idea = orm.db.relationship("Idea", backref=orm.db.backref('comments', order_by=id))
    description = orm.db.Column(Text())

    @staticmethod
    def comments_count(idea_id):
        return orm.db.session.query(func.count(Comment.id)).filter(Comment.idea_id == idea_id).scalar()

class Watcher(orm.Base, Timestampable, Serializable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    user_id = orm.db.Column(orm.db.ForeignKey('user.id'))
    idea_id = orm.db.Column(orm.db.ForeignKey('idea.id'))
    user = orm.db.relationship("User", backref=orm.db.backref('watching_ideas', order_by=id))
    idea = orm.db.relationship("Idea", backref=orm.db.backref('watchers', order_by=id))

    @staticmethod
    def user_watch(user_id, idea_id):
        return orm.db.session.query(func.count(Watcher.id)).filter(Watcher.user_id == user_id, Watcher.idea_id==idea_id).scalar() > 0

    @staticmethod
    def get_user_ideas(user_id):
        subq = orm.db.session.query(Watcher.idea_id).filter(Watcher.user_id == user_id)
        ideas = Idea.query.filter(Idea.id.in_(subq), Idea.deleted == False)
        return ideas

    @staticmethod
    def get_user_ideas_ids(user_id):
        subq = orm.db.session.query(Watcher.idea_id).filter(Watcher.user_id == user_id)
        ideas = orm.db.session.query(Idea.id).filter(Idea.deleted == False, Idea.id.in_(subq))
        return ideas

    @staticmethod
    def get_user_ideas_count(user_id):
        subq = orm.db.session.query(Watcher.idea_id).filter(Watcher.user_id == user_id)
        return orm.db.session.query(func.count(Idea.id)).filter(Idea.deleted == False, Idea.id.in_(subq)).scalar()

    @staticmethod
    def get_idea_watchers_count(idea_id):
        return orm.db.session.query(func.count(Watcher.id)).filter(Watcher.idea_id == idea_id).scalar()

class Follower(orm.Base, Timestampable, Serializable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    follower_id = orm.db.Column(orm.db.ForeignKey('user.id'))
    user_id = orm.db.Column(orm.db.ForeignKey('user.id'))
    user = orm.db.relationship("User", backref=orm.db.backref('followers', order_by=id), foreign_keys = user_id)
    follower = orm.db.relationship("User", backref=orm.db.backref('following', order_by=id), foreign_keys = follower_id)

    @staticmethod
    def user_follow(user_id, follower_id):
        return orm.db.session.query(func.count(Follower.id)).filter(Follower.follower_id == follower_id, Follower.user_id == user_id).scalar()  > 0

    @staticmethod
    def get_followers(user_id):
        return Follower.query.filter(Follower.user_id == user_id).options(joinedload("user"),joinedload("user.profile"))

    @staticmethod
    def get_followers_count(user_id):
        return orm.db.session.query(func.count(Follower.follower_id)).filter(Follower.user_id == user_id).scalar()

    @staticmethod
    def get_following(user_id):
        return Follower.query.filter(Follower.follower_id == user_id).options(joinedload("follower"),joinedload("follower.profile"))

    @staticmethod
    def get_following_count(user_id):
        return orm.db.session.query(func.count(Follower.user_id)).filter(Follower.follower_id == user_id).scalar()

    @staticmethod
    def get_friend_ids(user_id):
        followes = orm.db.session.query(Follower.follower_id).filter(Follower.user_id == user_id)
        friends = orm.db.session.query(Follower.user_id).filter(Follower.follower_id.in_(followes))
        return friends

class Like(orm.Base):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    user_id = orm.db.Column(orm.db.ForeignKey('user.id'))
    entity_id = orm.db.Column(orm.db.Integer)
    entity_class = orm.db.Column(orm.db.String(255))

    @staticmethod
    def get_entity_class(name):
        import sys
        return getattr(sys.modules["model.entity"],name)
        #return m

    @staticmethod
    def likes_count_for_entity(entity_class_name, entity_id):
        return orm.db.session.query(func.count(Like.user_id)).filter(Like.entity_class == entity_class_name, Like.entity_id == entity_id).scalar()

    @staticmethod
    def user_like(user_id, entity_class_name, entity_id):
        return orm.db.session.query(func.count(Like.user_id)).filter(Like.entity_class == entity_class_name,Like.entity_id == entity_id, Like.user_id == user_id).scalar() > 0

    @staticmethod
    def like_entity(entity_id, entity_class_name):
        entity_class = Like.get_entity_class(entity_class_name)
        entity = entity_class.query.get(entity_id)
        if not entity:
            raise NotFound()

        oldLike = Like.query.filter(Like.entity_class == entity_class_name, Like.entity_id == entity_id).first()

        if not oldLike:
            like = Like()
            like.user_id = current_user.id
            like.entity_class = entity_class_name
            like.entity_id = entity.id
            orm.db.session.add(like)
        else:
           orm.db.session.delete(oldLike)

        orm.db.session.commit()

        likes = Like.likes_count_for_entity(entity_class_name, entity_id)
        return likes


class NewsLog(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    user_id = orm.db.Column(orm.db.ForeignKey('user.id'))
    entity_id = orm.db.Column(orm.db.Integer)
    entity_class = orm.db.Column(orm.db.String(255))
    # user_id creates idea, or added comment
    type = orm.db.Column(orm.db.String(255))

class Competition(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    title = orm.db.Column(orm.db.String(255))
    description = orm.db.Column(Text())
    start_date = Column(DateTime(), nullable=False, default=datetime.now)
    end_date = Column(DateTime(), nullable=False, default=datetime.now)
    def __unicode__(self):
        return self.title



images_profiles = orm.db.Table('images_profiles',
        orm.db.Column('image_id', orm.db.Integer(), orm.db.ForeignKey('image.id')),
        orm.db.Column('user_profile_id', orm.db.Integer(), orm.db.ForeignKey('user_profile.id')))

images_ideas = orm.db.Table('images_ideas',
        orm.db.Column('image_id', orm.db.Integer(), orm.db.ForeignKey('image.id')),
        orm.db.Column('idea_id', orm.db.Integer(), orm.db.ForeignKey('idea.id')))

images_competitions = orm.db.Table('images_competitions',
        orm.db.Column('image_id', orm.db.Integer(), orm.db.ForeignKey('image.id')),
        orm.db.Column('competition_id', orm.db.Integer(), orm.db.ForeignKey('competition.id')))



class Image(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    path = orm.db.Column(orm.db.String(255))
    profiles = orm.db.relationship('UserProfile', secondary=images_profiles,
                            backref=orm.db.backref('images', lazy='dynamic'))
    ideas = orm.db.relationship('Idea', secondary=images_ideas,
                            backref=orm.db.backref('images', lazy='dynamic'))
    competitions = orm.db.relationship('Competition', secondary=images_competitions,
                            backref=orm.db.backref('images', lazy='dynamic'))


class Video(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    blob_key = orm.db.Column(orm.db.String(254))
    youtube_id = orm.db.Column(orm.db.String(254))
    is_promo = orm.db.Column(orm.db.Boolean())

    user_id = orm.db.Column(orm.db.ForeignKey('user.id'))
    idea_id = orm.db.Column(orm.db.ForeignKey('idea.id'))
    user = orm.db.relationship("User", backref=orm.db.backref('videos', order_by=id))
    idea = orm.db.relationship("Idea", backref=orm.db.backref('videos', order_by=id))


class PromoVideo(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)



class UserFeed(orm.Base, Serializable, Timestampable):
    """
    This class represents users feed.
    """
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    #id of user this feed belongs to
    user_id = orm.db.Column(orm.db.ForeignKey('user.id'))

    # Entity id on which action was performed eg Idea or User or Comment
    entity_class = orm.db.Column(orm.db.String(255))
    entity_id = orm.db.Column(orm.db.Integer)

    #type of action
    # 1 - Comment added
    # 2 - Some one begin watch (ideas)
    # 3 - Some one stop watch (ideas)
    # 4 - Some one begin follow (users)
    # 5 - Some one stops follow (users)
    # 6 - Some one creates idea
    action_type = orm.db.Column(orm.db.Integer)

    # id of user who performs this action
    action_performer = orm.db.Column(orm.db.Integer)

    # here will e placed compilled data of this action serialized as json
    # eg {user_id: 7, nickname: "User 1", action:1, idea_id: 1, idea_title:"My creat idea"
    data = orm.db.Column(UnicodeText())


class UserAccount(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    user_id = orm.db.Column(orm.db.ForeignKey('user.id'))
    amount = orm.db.Column(orm.db.Integer)


class IdeaAccount(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    idea_id = orm.db.Column(orm.db.ForeignKey('idea.id'))
    amount = orm.db.Column(orm.db.Integer)


class KudosTransaction(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    user_account_id = orm.db.Column(orm.db.ForeignKey('user_account.id'))
    idea_account_id = orm.db.Column(orm.db.ForeignKey('idea_account.id'))
    amount = orm.db.Column(orm.db.Integer)
    user_account = orm.db.relationship("UserAccount", backref=orm.db.backref('transactions', order_by=id))
    idea_account = orm.db.relationship("IdeaAccount", backref=orm.db.backref('transactions', order_by=id))


class RatingHistory(orm.Base, Serializable, Timestampable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    rank = orm.db.Column(orm.db.Integer)
    idea_id = orm.db.Column(orm.db.ForeignKey('idea.id'))
    idea = orm.db.relationship("Idea", backref=orm.db.backref('ratings'))
    competition_id = orm.db.Column(orm.db.ForeignKey('competition.id'))
    competition = orm.db.relationship("Competition", backref=orm.db.backref('ratings'))

    @staticmethod
    def get_for_idea(idea_id):
        rating = RatingHistory.query.filter(RatingHistory.idea_id == idea_id)\
            .order_by(RatingHistory.date_created.desc()).limit(1).first()
        if rating:
            return rating.rank
        return 0


#AGGREGATES
class ProfileExtra(orm.Base, Serializable):
    id = orm.db.Column(orm.db.Integer, primary_key=True)
    profile_id = orm.db.Column(orm.db.ForeignKey('user_profile.id'))
    profile = orm.db.relationship("UserProfile", backref=orm.db.backref('extra', uselist=False))

    invested_ideas = orm.db.Column(orm.db.Integer)
    kudos_amount = orm.db.Column(orm.db.Integer)
    ideas_count = orm.db.Column(orm.db.Integer)
    watching_ideas = orm.db.Column(orm.db.Integer)
    following_count = orm.db.Column(orm.db.Integer)
    followers_count = orm.db.Column(orm.db.Integer)
    email = orm.db.Column(orm.db.String(254))


def aggregate_profile_extra(profile_id):
    profile = UserProfile.query.get(profile_id)

    if not profile:
        return

    followers = Follower.get_followers_count(profile.user_id)

    following = Follower.get_following_count(profile.user_id)

    ideas = Watcher.get_user_ideas_count(profile.user_id)

    kudos = 0
    invested_ideas = 0
    if profile.user.account:
        kudos = profile.user.account.amount
        from sqlalchemy import distinct
        invested_ideas = orm.db.session.query(func.count(distinct(KudosTransaction.idea_account_id)))\
            .filter(KudosTransaction.user_account_id == profile.user.account.id)\
            .scalar()


    extra = ProfileExtra.query.filter(ProfileExtra.profile_id == profile_id).first()
    if extra is None:
        extra = ProfileExtra()
        extra.profile_id = profile_id

    extra.invested_ideas = invested_ideas
    extra.kudos_amount = kudos
    extra.ideas_count = Idea.get_user_ideas_count(profile.user_id)
    extra.watching_ideas = ideas
    extra.following_count = following
    extra.followers_count = followers
    extra.email = profile.user.email
    orm.db.session.add(extra)
    orm.db.session.commit()


def track_comment(comment_id):
    comment = Comment.query.get(comment_id)
    if not comment or comment.idea is None:
        return


    data = {}
    data['action'] = 1
    data['performer_id'] = comment.user_id
    if comment.user.profile:
        data['performer'] = comment.user.profile.to_dict()
    data['idea_id'] = comment.idea_id
    if comment.idea is not None:
        data['comments_count'] = comment.idea.comments_count()
        data['idea_title'] = comment.idea.title
    data['idea_image_id'] = get_idea_image_id(comment.idea)
    data['comment'] = comment.description
    data['comment_id'] = comment.id




    user_ids = []
    #notify ideas author
    if comment.idea.user_id <> comment.user_id:
        user_ids.append(comment.idea.user_id)

    #notify all idea watchers
    watchers = comment.idea.watchers
    for watcher in watchers:
        if watcher.user_id <> comment.user_id:
            user_ids.append(watcher.user_id)

    #notify comment author followers that he comment idea

    followers = Follower.get_followers(comment.user_id)
    for follower in followers:
        if follower.follower_id <> comment.user_id:
            user_ids.append(follower.follower_id)

    for uid in set(user_ids):
        feed = UserFeed()
        feed.date_created = comment.date_created
        feed.action_type = 1
        feed.entity_class = "Idea"
        feed.entity_id = comment.idea_id
        feed.data = dumps(data)
        feed.action_performer = comment.user_id
        feed.user_id = uid
        orm.db.session.add(feed)

    orm.db.session.commit()


def track_idea_create(idea_id):
    idea = Idea.query.get(idea_id)
    if not idea:
        return

    data = {}
    data['action'] = 6
    data['performer_id'] = idea.user_id
    if idea.user.profile:
        data['performer'] = idea.user.profile.to_dict()
    data['idea_id'] = idea.id
    data['idea_title'] = idea.title
    data['idea_description'] = idea.description
    data['idea_image_id'] = get_idea_image_id(idea)

    uids = []

    #notify idea author followers that he creates new idea

    followers = Follower.get_followers(idea.user_id)
    for follower in followers:
        if follower.follower_id <> idea.user_id:
            uids.append(follower.follower_id)

    #notify idea watchers
    watchers = idea.watchers
    for watcher in watchers:
        if watcher.user_id <> idea.user_id:
            uids.append(watcher.user_id)

    for uid in set(uids):
        feed = UserFeed()
        feed.date_created = idea.date_created
        feed.action_type = 6
        feed.entity_class = "Idea"
        feed.entity_id = idea.id
        feed.data = dumps(data)
        feed.action_performer = idea.user_id
        #feed owner
        feed.user_id = uid
        orm.db.session.add(feed)

    orm.db.session.commit()

def track_watch_idea(watch_id):
    watcher = Watcher.query.get(watch_id)
    if not watcher or watcher.idea is None:
        return

    data = {}
    data['action'] = 2
    data['performer_id'] = watcher.user_id
    if watcher.user.profile:
        data['performer'] = watcher.user.profile.to_dict()
    data['idea_id'] = watcher.idea_id
    data['idea_title'] = watcher.idea.title
    data['idea_description'] = watcher.idea.description
    data['idea_image_id'] = get_idea_image_id(watcher.idea)

    uids = []

    #notify idea author
    uids.append(watcher.idea.user_id)

    #notify idea author followers
    followers = Follower.get_followers(watcher.idea.user_id)
    for follower in followers:
        if follower.user_id <> watcher.user_id:
            uids.append(follower.follower_id)

    for uid in set(uids):
        feed = UserFeed()
        feed.date_created = watcher.date_created
        feed.action_type = 2
        feed.entity_class = "Idea"
        feed.entity_id = watcher.idea.id
        feed.data = dumps(data)
        feed.action_performer = watcher.user_id
        #feed owner
        feed.user_id = uid
        orm.db.session.add(feed)

    orm.db.session.commit()

def track_video_added(video_id):
    video = Video.query.get(video_id)
    if not video:
        return

    data = {}
    data['action'] = 2
    data['performer_id'] = video.user_id
    if video.user.profile:
        data['performer'] = video.user.profile.to_dict()
    data['idea_id'] = video.idea_id
    data['idea_title'] = video.idea.title
    data['idea_description'] = video.idea.description
    data['idea_image_id'] = get_idea_image_id(video.idea)

    uids = []

    #notify idea author

    #notify idea author followers
    followers = Follower.get_followers(video.user_id)
    for follower in followers:
        if follower.follower_id <> video.user_id:
            uids.append(follower.follower_id)

    #notify idea watchers
    watchers = video.idea.watchers
    for watcher in watchers:
        if watcher.user_id <> video.user_id:
            uids.append(watcher.user_id)



def get_idea_image_id(idea):
    if idea is None:
        return
    for img in idea.images:
        if img.path.endswith("_thumb"):
            return img.path


def create_tables():
    orm.db.create_all()
    orm.db.session.commit()



def generate_feeds():
    comments = Comment.query.all()
    for comment in comments:
        track_comment(comment.id)

    ideas = Idea.query.all()

    for idea in ideas:
        track_idea_create(idea_id=idea.id)

    watchers = Watcher.query.all()

    for watcher in watchers:
        track_watch_idea(watcher.id)


def build_rating(comp_id):
    competition = Competition.query.get(comp_id)
    if not competition:
        raise NotFound()

    ideas = orm.db.session.query(Idea.id).join(IdeaAccount).filter(Idea.competition == competition, Idea.deleted == False).order_by(IdeaAccount.amount.desc())
    rank = 0
    created = datetime.now()
    for idea in ideas:
        rank += 1
        rating = RatingHistory()
        rating.date_created = created
        rating.rank = rank
        rating.idea_id = idea.id
        rating.competition_id = comp_id
        orm.db.session.add(rating)

    orm.db.session.commit()
