sexualviolence
==============

This is a Google App Engine project and requires the App Engine SDK

Instructions to download and install:

https://developers.google.com/appengine/downloads

Requirements

 - MySQLdb
 
To Run
======

cd to application root

gcloud auth login

dev_appserver.py --mysql_host=<host> --mysql_user=<user> --mysql_password=<password> .

for use Cloud SQL instance make sure that in application console instance configured for accessing from your network
 
Site will then be visible http:localhost:8080
 
If you have a redirect problem, then use

gcloud auth login --no-launch-browser
 
If you are running multiple App Engine projects, use

gcloud config set project ideas-britain
 
Every push to github automatically deploys code. Site will update within 2-3 minutes.
Dev site is here: https://ideas-britain.appspot.com/


Setup Database
==============

mysql -u root -p

Create a database named "ideasbritain":

 mysql> CREATE DATABASE ibdb;

Allow the user "ideasbritain" to connect with password "localdb":

 mysql> GRANT ALL PRIVILEGES ON ibdb.* TO 'ideasbritain'@'localhost' IDENTIFIED BY 'ideasbritain';

Import the provided schema.sqll to create tables in the new database

 mysql --user=ideasbritain --password=localdb --database=ibdb < sql/initial_schema.sql
 
 Then run all the SQL diff files, this will patch the DB and add test data.
 
 
 
Data API
========


/api/login - log user in or raise error.
method: POST
secured: false
Requires data to be posted(eg POST method) and to be valid json string

example request
{
    "email":"test@test.sa",
    "password":"test1"
}

example response
{
  "data": {
    "user_token": "WyIxIiwiMDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjYiXQ.B1DIhg.WCOp9LfBqQE0MB0Er0QzWjgzU3k"
  },
  "error": false,
  "status_code": 200
}

User token should be sent via http header "user-token" for secured api calls

logout
/api/logout
method: GET
secure: true

Check nickname
/api/check_nickname
method:POST
secure: false

Password restore
/api/restore_password
method:POST
secure:false
ex request
{
    email:"test@test.su"
}
if no user found then 404

Register new user
/api/register
method:POST
secure: false

example request
{
    "email":"kozloffsky4@test.sa",
    "password":"test123",
    "password_confirm":"test123",
    "first_name":"Mike",
    "last_name":"Oz",
    "nickname":"mike_oz1",
    "image":"base64ecodedimage"
}

Show user profile
method:GET
/api/profile/<int:profile_id>
example resoponse

    {
       "data":
       {
           "profile":
           {
               "date_created": "Fri, 28 Nov 2014 12:03:20 GMT",
               "date_updated": "Fri, 28 Nov 2014 12:03:20 GMT",
               "description": null,
               "first_name": "Mike",
               "id": 1,
               "last_name": "Oz",
               "nickname": "mike_oz",
               "user_id": 6
           }
       },
       "error": false,
       "status_code": 200
    }

List Ideas 

/api/idea/<int:page>/<int:per_page>/?filter=<str:filter_name>
Return list of ideas filtered
filter=comments - is for most commented
filter=friends - show only friends ideas
if filter is not set then show all ideas by creation_date


List of current user ideas
/api/idea/my/<int:page>/<int:per_page>/
secured:True
method: GET

List for watching ideas by user
/api/idea/watching/<int:user>/<int:page>/<int:per_page>/
secured: True
method: GET

get Idea info
/api/idea/<int:idea_id>
secure: false
method: GET
"data": {
    "comments": [
      {
        "likes_count": 0, 
        "date_created": "Thu, 18 Dec 2014 14:36:10 GMT", 
        "date_updated": "Thu, 18 Dec 2014 14:36:10 GMT", 
        "description": "Test", 
        "id": 8, 
        "idea_id": 2, 
        "user_id": 8, 
        "user_like": false
      }, 
      {
        "likes_count": 0, 
        "date_created": "Thu, 18 Dec 2014 14:11:14 GMT", 
        "date_updated": "Thu, 18 Dec 2014 14:11:03 GMT", 
        "description": "wedfds dsgg", 
        "id": 6, 
        "idea_id": 2, 
        "user_id": 6, 
        "user_like": false
      }, 
      {
        "likes_count": 0, 
        "date_created": "Thu, 18 Dec 2014 14:11:11 GMT", 
        "date_updated": "Thu, 18 Dec 2014 14:11:01 GMT", 
        "description": "jhghjgjhgjg", 
        "id": 5, 
        "idea_id": 2, 
        "user_id": 6, 
        "user_like": false
      }
    ], 
    "comments_count": 8, 
    "idea": {
      "competition_id": 1, 
      "date_created": "Thu, 18 Dec 2014 14:03:06 GMT", 
      "date_updated": "Thu, 18 Dec 2014 14:03:28 GMT", 
      "description": "I love the new update it was ", 
      "id": 2, 
      "title": "jgjgjgjgj", 
      "user_id": 8
    }, 
    "user": {
      "date_created": "Mon, 08 Dec 2014 12:10:20 GMT", 
      "date_updated": "Mon, 08 Dec 2014 12:10:20 GMT", 
      "description": null, 
      "first_name": "Alexa", 
      "id": 3, 
      "last_name": "K", 
      "nickname": "alexa_k", 
      "user_id": 8
    }, 
    "watchers": 1,
    "user_watch": true|false
  }, 
  "error": false, 
  "status_code": 200
}

create new idea
/api/idea
secured: true
method: PUT
{
    user_id:int
    title:string
    description:string
    images:[
        {
            data:base64ecoded_string,
            is_thumb:bool
        }
    }
    
}

update idea
/api/idea/<int:idea_id>
secured: true
method: POST

add comment to idea
/api/comment/<int:idea_id>
secured: true
method: POST
{
    user_id:int
    idea_id:int
    description:string
}

comments list
/api/comment/<int:idea_id>/<int:page>/<int:per_page>
secured: false
method: GET



IMAGES
/image/profile/<int:image_id>/<int:is_thumb>
return image associated with profile id is_thumb is 0, thumbnail otherwise

/image/<int:img_id>
return image with given id, for profile images better use /image/profile/ 

PROFILE
Show Profile
/api/profile/<int:profile_id>
method: GET
secured: FALSE

Update profile
/api/profile/<int:profile_id>
method: POST
secured: TRUE
    